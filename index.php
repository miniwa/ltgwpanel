<?php
session_start();

//logged_in koder:
//-2 = ej inloggad, redirect från annan sida (logga in först)
//-1 = fel lösen
// 0 = ej inloggad
// 1 = inloggad

//nya koder:
// 0 = ej inloggad
// 1 = inloggad
// 2 = fel lösen
// 3 = ej inloggad, redirect från annan sida (logga in först)

if (isset($_SESSION["logged_in"]) == false) {
    $_SESSION["logged_in"] = 0;
}
if (isset($_COOKIE["darkmode"]) == false) {
    setcookie("darkmode","0",time() + (10 * 365 * 24 * 60 * 60), '/admin/');
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if ($_POST['username'] == "testuser1337") {
          header("Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ");
          die();
  }
    if (pam_auth($_POST['username'], $_POST['password']) == true) {
        $_SESSION["logged_in"] = 1;
        $_SESSION["user"] = $_POST['username'];
    } else {
        $_SESSION["logged_in"] = 2;
        unset($_SESSION["user"]); //tror fan detta ens kan hända BUT IM LEAVING IT /dab
    }
}

if ($_SESSION["user"] == "jack" || $_SESSION["user"] == "washam") {
    $_SESSION["admin"] = 1;
} else {
    $_SESSION["admin"] = 0;
}


if ($_SESSION["logged_in"] == 1) {
    header("Location: https://lindholmen.club/admin/home");
    die();
}
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ltgwPanel</title>
        <meta name="description" content="Logga in...">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/admin/css/normalize.min.css">
        <link rel="stylesheet" href="/admin/css/main.css">
        <link rel="stylesheet" href="/admin/css/login.css">
        <link rel="stylesheet" href="/admin/css/cookie.css">
        <?php if (isset($_COOKIE["darkmode"]) == true && $_COOKIE["darkmode"] == "1") {echo '<link rel="stylesheet" href="/admin/css/darkmode.css">';}?>
        <script src="/admin/js/cookie.js"></script>

    </head>
    <body>
    <div id="loginpage">
        <div id="loginbox">
            <form action="/admin/" method="post">
                <p id="logintitle">ltgwPanel</p>
                <p id="loginsubtitle"><?php
                    switch ($_SESSION["logged_in"]) {
                        case 3:
                            echo "logga in först...";
                            $_SESSION["logged_in"] = 0;
                            break;
                        case 2:
                            echo "försök igen?";
                            $_SESSION["logged_in"] = 0;
                            break;
                        case 0:
                            echo "vänligen logga in...";
                            break;
                        case 1:
                            echo "(du är inloggad)";
                            break;
                    }
                    ?></p>
                <input id="loginname" type="text" name="username" placeholder="namn" required><input id="loginpassword" type="password" name="password" placeholder="lösen" required><br>
                <input id="loginsubmit" type="submit" value="logga in">
            </form>
        </div>
    </div>
    </body>
</html>
