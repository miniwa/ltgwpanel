<?php
session_start();

require $_SERVER['DOCUMENT_ROOT']."/admin/include/header.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['darkmode'])) {
    if ($_POST['darkmode'] == 'on') {
        setcookie("darkmode","1",time()+(10 * 365 * 24 * 60 * 60), '/admin/');
    } else {
        setcookie("darkmode","0",time()+(10 * 365 * 24 * 60 * 60), '/admin/');
    }
    header("Location: https://lindholmen.club/admin/");
    die();
    }
    if (isset($_POST['oldpw']) == true && isset($_POST['newpw1']) == true && isset($_POST['newpw2']) == true) {
        if ($_POST['newpw1'] == $_POST['newpw2']) {
            if (pam_auth($_SESSION['user'], $_POST['oldpw']) == true) {
                //change the pw
                require $_SERVER['DOCUMENT_ROOT']."/admin/scripts/chpasswd.php";
                session_unset();
                session_destroy();
                header("Location: https://lindholmen.club/admin/");
                die();
            } else {
                echo "you fucked up";
            }
        } else {
            echo "you fucked up";
        }
    }
}

include $_SERVER['DOCUMENT_ROOT']."/admin/include/layout1.php";
?>
<h1>Inställningar</h1>
<form action="/admin/settings/" method="post">
    Mörkt läge <input type="radio" name="darkmode" value="off" checked> Av <input type="radio" name="darkmode" value="on" 
    <?php
    if ($_COOKIE["darkmode"] == "1") {echo "checked";}
    ?>
    > På<br>
    <input type="submit" value="Spara">
</form>
<form action="/admin/settings/" method="post">
    Ändra lösenord<br>
    <input type="password" name="oldpw" placeholder="Gammalt lösenord" required><br>
    <input type="password" name="newpw1" placeholder="Nytt lösenord" required><br>
    <input type="password" name="newpw2" placeholder="Nytt lösenord (igen)" required><br>
    <input type="submit" value="Ändra">
</form>
<?php
if ($_SESSION["admin"] == 1) {
  if (isset($_SESSION['msgcolor'])) {
    include $_SERVER['DOCUMENT_ROOT']."/admin/include/infobar.php";
  }
  echo '
  <form action="/admin/scripts/chuser.php" method="post">
    Sudo<br>
    <input type="text" name="osarrr" placeholder="Användare" required><br>
    <input type="submit" value="Byt användare">
  </form>
  ';
}

include $_SERVER['DOCUMENT_ROOT']."/admin/include/layout2.php";
?>