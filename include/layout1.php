<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>ltgwPanel</title>
        <meta name="description" content="Logga in...">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="/admin/css/normalize.min.css">
        <link rel="stylesheet" href="/admin/css/main.css">
        <link rel="stylesheet" href="/admin/css/cookie.css">
        <?php if ($_COOKIE["darkmode"] == "1") {echo '<link rel="stylesheet" href="/admin/css/darkmode.css">';}?>
        <script src="/admin/js/cookie.js"></script>

    </head>
    <body>
    <div id="container">
        <header>
            <p id="headerleft"><a href="/admin/home/">ltgwPanel</a></p>
		    <div id="headerright">
			    <p>
            <?php
            echo '<a href="/admin/status">status</a> - ';
            echo '<a href="/admin/mysite/">min sida</a> - ';
            echo '<a href="/admin/guides/">guider</a> - ';
            if ($_SESSION['admin'] == 1) {
              echo '<a href="/admin/users/">användare</a> - ';
            } else {
              echo '<a href="/admin/ltgchat/">ltgchat</a> - ';
            }
            echo '<a href="/admin/settings/">inställningar</a> - ';
            echo '<a href="/admin/logout/">logga ut</a>';
            ?>
			    </p>
		</div>
        </header>
        <section>
            <aside>
            <?php
                echo "<p id='topsidebar'>välkommen ".$_SESSION["user"]."<br>";
                echo "disk fri: ".substr(round(disk_free_space("/")/disk_total_space("/"), 2),2)."%<br>";
                echo "ram fritt: ".substr((round(shell_exec('free -m | tail -n 2 | head -n 1 | tail -c 4')/433, 3)),2,2)."%<br>";
                echo "cpu användning: ".shell_exec("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}' | head -c 4")."%<br><br></p>";
                if ($_SESSION['admin'] == 1) {
                  echo "<p id='bottomsidebar'>";
                  echo "<a class='sidebarbutton' href='/admin/scripts/reslttpd.php'>starta om lighttpd</a><br><br>";
                  echo "<a class='sidebarbutton' href='/admin/scripts/resserve.php'>starta om server</a><br><br>";
                  if ($_SESSION['user'] == "jack") {
                    echo "<a class='sidebarbutton sidebarbuttonRED' href='/admin/scripts/shutfuck.php'>stäng av server</a><br><br>";
                  }
                  echo "<a class='sidebarbutton sidebarbuttonRED' href='/admin/scripts/shutlttpd.php'>stäng av webbserver</a>";
                  echo "</p>";
                }
            ?>
            </aside>
            <article>