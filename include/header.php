<?php
if (isset($_SESSION["logged_in"]) == false || $_SESSION["logged_in"] != 1) {
    $_SESSION["logged_in"] = 3;
    header("Location: https://lindholmen.club/admin/");
    die();
}
?>