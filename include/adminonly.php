<?php
if ($_SESSION["admin"] != 1) {
    header("Location: https://lindholmen.club/admin/home/");
    die();
}
?>